const supertest = require('supertest');

const
    server = 'localhost',
    port   = 8080,
    url    = `http://${server}:${port}`;
const
    timeOutMilliSeconds = 200;

module.exports = {
    url,
    apiServer:supertest.agent(url),
    timeOutMilliSeconds
}
