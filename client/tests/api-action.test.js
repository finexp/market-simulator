const request = require('supertest');
const {url} = require('./server.js');

function  isGoodHttpCode (code) {
    return code == 200;
}

expect.extend({
    toBeValidResponse(response) {
        // {statusCode,statusCode,error}
        // TODO: will ignore error code for the moment (re-use statusCode)
        // const errors = error ? "Som"
        // }
        // const {hasHttpCode,body} = response;
        // const
        //     hasHttpCode = response.statusCode!==undefined,
        //     hasError = response.body.error!==undefined,
        //     has isGoodHttpCode(hasHttpCode),
        //     pass = hasHttpCode && isGoodHttpCode(hasHttpCode) != hasError;
        // // const pass = hasHttpCode;
        // const text = `httpCode=${statusCode} error="${error}"`;

        // console.log('*****************************',response.statusCode,hasHttpCode,hasError);
        // console.log(Object.keys(response));

        const pass = true;

        return {
            message: () => '',
            pass,
        };

        // if (pass) {
        //     return {
        //         message: () => `expected: ${text}`,
        //         pass,
        // };
        // } else {
        //     return {
        //         message: () => `unexpected: ${text}`,
        //         pass,
        //     };
        // }
    },
});

expect.extend({
    toBeGoodResponse(response) {
        // if(arg!==undefined)         console.log(`The arg is ${JSON.stringify(arg)}`);
        // HttpCode is 2xx
        const hasGoodCode = Math.floor(response.statusCode/100)===2;
        const hasNoError = (!response.body) || (!response.body.error);
        const pass = hasGoodCode && hasNoError;
        const text = JSON.stringify(response.body);
        return {
            message: () => text,
            pass
        };
    },
});

describe ('root endpoint access', () => {
    it('should return a 200 status code', async () => {
        const response = await request(url).get('/');
        expect(response.statusCode).toBe(200);
    });
});

function seconds_since_epoch(){ return Math.floor( Date.now() / 1000 ) }
const testTime = seconds_since_epoch();

const reqAddQuotes = (marketId,quotesId) => ({
    version: {
        name: "SimpleBrownianMotionQuotesGenerator",
        number: 0
    },
    marketId,
    quotesId,
    timeStart:0,
    timeEnd:20,
    timeStep:1,
    calendarConversion:"",
    calendarTimeStart:"",
    drift:0.1,
    diffusion:1,
    stateStart:-1
});

function testQuotes (marketId,quotesId,justAdd=false) {
    it('should add a new quotesId', async () => {
        const response = await request(url)
            .post('/action')
            .send(reqAddQuotes(marketId,quotesId));
        if(!justAdd){
            expect(response).toBeValidResponse();
            expect(response).toBeGoodResponse();
        }
    });

    if(!justAdd){
        it('should NOT add the same quotesId again', async () => {
            const response = await request(url)
                .post('/action')
                .send(reqAddQuotes(marketId,quotesId));
            expect(response).toBeValidResponse();
            expect(response).not.toBeGoodResponse();
        });
    }
}

describe ('Actions', () => {
    const marketId = `TestMarketA-${testTime}`;
    it('should add a new marketId', async () => {
        const response = await request(url)
            .post('/action')
            .send({
                version: {
                    name: "AddMarket",
                    number: 0
                },
                marketId
            });
        expect(response).toBeValidResponse();
        expect(response).toBeGoodResponse();
    });

    it('should NOT add the SAME marketId twice', async () => {
        const response = await request(url)
            .post('/action')
            .send({
                version: {
                    name: "AddMarket",
                    number: 0
                },
                marketId
            });
        expect(response).toBeValidResponse();
        expect(response).not.toBeGoodResponse();
    });

    testQuotes(marketId,'quotesA');

    it('should delete marketId', async () => {
        const response = await request(url)
            .post('/action')
            .send({
                version: {
                    name: "DeleteMarket",
                    number: 0
                },
                marketId
            })
        expect(response).toBeValidResponse();
        expect(response).toBeGoodResponse();
    });

    it('should NOT delete the same marketId twice', async () => {
        const response = await request(url)
            .post('/action')
            .send({
                version: {
                    name: "DeleteMarket",
                    number: 0
                },
                marketId
            })
        expect(response).toBeValidResponse();
        expect(response).not.toBeGoodResponse();
    });
});

describe ('GET/PUT/DEL requests', () => {
    const marketId = `TestMarketB-${testTime}`;
    it('should get markets', async () => {
        const response = await request(url)
            .get('/markets');
        expect(response).toBeValidResponse();
        expect(response).toBeGoodResponse();
    });
    it('should add markets-id', async () => {
        const response = await request(url)
            .put(`/markets/${marketId}`);
        expect(response).toBeValidResponse();
        expect(response).toBeGoodResponse();
    });
    it('should not add the same markets-id twice', async () => {
        const response = await request(url)
            .put(`/markets/${marketId}`);
        expect(response).toBeValidResponse();
        expect(response).not.toBeGoodResponse();
    });

    testQuotes(marketId,'quotesB');

    it('should delete markets-id', async () => {
        const response = await request(url)
            .del(`/markets/${marketId}`);
        expect(response).toBeValidResponse();
        expect(response).toBeGoodResponse();
    });
    it('should NOT delete the same markets-id twice', async () => {
        const response = await request(url)
            .del(`/markets/${marketId}`);
        expect(response).toBeValidResponse();
        expect(response).not.toBeGoodResponse();
    });
});

describe ('GET/PUT/DEL requests on quotes', () => {
    const marketId = `TestMarketC-${testTime}`;
    it('should add markets-id', async () => {
        const response = await request(url)
            .put(`/markets/${marketId}`);
        expect(response).toBeValidResponse();
    });

    const
        quotesId1 = 'q1',
        quotesId2 = 'q2';
    testQuotes(marketId,quotesId1,true);
    testQuotes(marketId,quotesId2,true);

    it('should get list of quotesIds', async () => {
        const response = await request(url)
            .get(`/markets/${marketId}/quotes`);
        expect(response).toBeValidResponse();
        expect(response).toBeGoodResponse();
    });

    it('should get list of quotes', async () => {
        const response = await request(url)
            .get(`/markets/${marketId}/quotes/${quotesId1}`);
        expect(response).toBeValidResponse();
        expect(response).toBeGoodResponse();
    });
});
