const {
	ReasonPhrases,
	StatusCodes,
	getReasonPhrase,
	getStatusCode,
} = require ('http-status-codes');

const {apiServer,timeOutMilliSeconds} = require('./server.js');

beforeAll(() => {
    // console.log('beforeAll');
},timeOutMilliSeconds);

afterAll(() => {
    // console.log('afterAll');
},timeOutMilliSeconds);

function seconds_since_epoch(){ return Math.floor( Date.now() / 1000 ) }

describe('Test generic API (with json)', () => {
    // const marketId = `AddQuotesTest-${seconds_since_epoch()}`;
    const marketId = `m1`;

    it('The server should be running', done => {
        apiServer
            .get('/')
            .expect(StatusCodes.OK)
            .end(function(err, res) {
                done(err);
            });
    });

/*
    it('should add a new marketId', done => {
        apiServer
            .get(`/markets/add/${marketId}`)
            .expect(StatusCodes.OK)
            .end(function(err, res) {
                done(err);
            });
    });
*/

    let qg = {
        "calendarConversion": "",
        "calendarTimeStart": "1970-01-01 01:00:00 +0100",
        marketId,
        "quoteId": "q1",
        "timeStart": 1.1,
        "timeEnd": 2.2,
        "timeStep": 0.1,
        "drift":-0.1,
        "diffusion":0.2,
        "stateStart":0.123456,
        "version": {
          "name": "SimpleBrownianMotionQuotesGenerator",
          "number": 0
        }
    };
/*
    it('SimpleBrownianMotionQuotesGenerator 1', done => {
        console.log('Sending',qg);
        apiServer
            .post('/action')
            .send(qg)
            .set('Accept', 'application/json')
            .expect(StatusCodes.OK)
            .expect('Content-Type', /json/)
            .end(function(err, res) {
                done(err);
            });
    });

    it('SimpleBrownianMotionQuotesGenerator version=-1 is not supported', done => {
        apiServer
            .post('/action')
            .send({...qg,version: {
                  name: "SimpleBrownianMotionQuotesGenerator",
                  number: -1
                }
            })
            .set('Accept', 'application/json')
            .expect(StatusCodes.INTERNAL_SERVER_ERROR)
            .expect('Content-Type', /text\/plain/)
            .end(function(err, res) {
                done(err);
            });
    });

    it('SimpleBrownianMotionQuotesGenerator "version" must be present', done => {
        let copy = {...qg};
        delete copy.version;
        apiServer
            .post('/action')
            .send(copy)
            .set('Accept', 'application/json')
            .expect(StatusCodes.INTERNAL_SERVER_ERROR)
            .expect('Content-Type', /text\/plain/)
            .end(function(err, res) {
                done(err);
            });
    });

    it('should fail with malformed version', done => {
        apiServer
            .post('/action')
            .send({...qg,version:0})
            .set('Accept', 'application/json')
            .expect(StatusCodes.INTERNAL_SERVER_ERROR)
            .expect('Content-Type', /text\/plain/)
            .end(function(err, res) {
                done(err);
            });
    });
*/
})
