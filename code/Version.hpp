#pragma once

#include <iostream>

#include "fmt/format.h"

struct Version {
    std::string name;
    int number {0};
    bool operator == (const Version &) const = default;
    bool operator != (const Version &) const = default;
};

inline
std::ostream & operator << (std::ostream &os, const Version &o) {
    os << o.name << "-" << o.number;
    return os;
}

template <> struct fmt::formatter<Version> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end && *it != '}') throw format_error("invalid format");
        return it;
    }

    template <typename FormatContext>
    auto format(const Version& v, FormatContext& ctx) -> decltype(ctx.out()) {
        return format_to(ctx.out(),"Version({} {})",v.name,v.number);
    }
};
