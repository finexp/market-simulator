#pragma once

#include <chrono>
#include "Market.hpp"
#include "log.hpp"

struct Session {
    using _ID     = std::string;
    using _Time   = std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds>;
    using _Quote  = Quote<_Time,double>;
    using _Quotes = std::set<_Quote>;
    using _Market = Market<_ID,_Quote>;

    std::map<_ID,_Market> markets;

    Session (void);

    _Market & GetMarket (const _ID &marketId) {
        auto market_it = markets.find(marketId);
        if(market_it==markets.end())
            throw_exception("{} market not found: {}",__func__,marketId);
        return market_it->second;
    }

    const _Market & GetMarket (const _ID &marketId) const {
        auto market_it = markets.find(marketId);
        if(market_it==markets.end())
            throw_exception("{} market not found: {}",__func__,marketId);
        return market_it->second;
    }

    void AddQuotesGeneratorInfo(const _ID& marketId, const _ID& quotesID, std::string && text);
    void AddQuotes(const _ID& marketId, const _ID& quotesID, _Quotes && quotes);
    void AddMarket(const _ID& marketId);
    void DeleteMarket(const _ID& marketId);
};
