#pragma once

#include <random>

#include "log.hpp"

class RandomNormalDistribution {
public:

    std::default_random_engine engine;
    std::normal_distribution<> norm;

    RandomNormalDistribution (
        double mean = 0,
        double sigma = 1,
        int seed = 1
    )
        : engine (seed)
        , norm (mean, sigma)
    {}

    double operator () (void) {
        return norm (engine);
    }
};

template <typename S>
struct ProcessState {
    using State = S;
    double time;
    State state;
};

template <> template <typename S> struct fmt::formatter<ProcessState<S>> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end && *it != '}') throw format_error("invalid format");
        return it;
    }

    template <typename FormatContext>
    auto format(const ProcessState<S>& p, FormatContext& ctx) -> decltype(ctx.out()) {
        return format_to(ctx.out(),"(t={},{})",p.time,p.state);
    }
};

template <typename S>
std::ostream & operator << (std::ostream &os, const ProcessState<S> &p) {
    os << fmt::format("{}",p);
    return os;
}

template <typename S>
class Process {
public:
    using State = S;
protected:
    ProcessState <State> ps;
public:
    virtual ~Process (void) {}

    Process (
        double time = 0,
        const State & state = {}
    )
    : ps {time,state} {}
    
    double GetTime (void) const {return ps.time;}
    State GetState (void) const {return ps.state;}

    virtual ProcessState<State> & Advance (double dt) = 0;
    virtual ProcessState<State> & Advance (void) = 0;
    const ProcessState <State> & operator () (void) const {return ps;}
};

class BrownianMotionProcess: public Process<double> {
public:

    RandomNormalDistribution r;
    double drift;
    double diffusion;
    double dt;

    BrownianMotionProcess (
        double t0 = 0,
        double start = 0,
        double _drift = 0,
        double _diffusion = 1,
        double _dt = 1,
        int seed = 1
    )
        : Process<double> {t0,start}
        , r (0,1,seed)
        , drift {_drift}
        , diffusion {_diffusion}
        , dt {_dt}
    {}

    ProcessState<double> & Advance (double dt) override {
        debug("{}  drift={} diffusion={} dt={} state={}",__func__,drift,diffusion,dt,this->ps);
        this->ps.time += dt;
        this->ps.state += drift*dt + diffusion*std::sqrt(dt)*r();
        debug("BM advance: {}",this->ps);
        return this->ps;
    }

    ProcessState<double> & Advance (void) override {
        debug("BM: forward advance()...",this->ps);
        return Advance(dt);
    }
};

class GeometricalBrownianMotionProcess: public Process<double> {
public:

    RandomNormalDistribution r;
    double drift;
    double diffusion;
    double dt;

    GeometricalBrownianMotionProcess (
        double t0 = 0,
        double start = 0,
        double _drift = 0,
        double _diffusion = 1,
        double _dt = 1,
        int seed = 1
    )
        : Process<double> {t0,start}
        , r (0,1,seed)
        , drift {_drift}
        , diffusion {_diffusion}
        , dt {_dt}
    {}

    ProcessState<double> & Advance (double dt) override {
        this->ps.time += dt;
        auto state_change = this->ps.state * (drift*dt + diffusion*std::sqrt(dt)*r());
        auto state_new = this->ps.state + state_change;
        this->ps.state = std::signbit(state_new)==std::signbit(this->ps.state) ? state_new : NAN;
        return this->ps;
    }

    ProcessState<double> & Advance (void) override {
        return Advance(dt);
    }
};

#if 0
class GeometricalBrownianMotionProcess: public Process {
public:

    RandomNormalDistribution r;
    double state;
    double drift;
    double diffusion;
    double dt;

    GeometricalBrownianMotionProcess (
        double t0 = 0,
        double start = 1,
        double _drift = 0,
        double _diffusion = 1,
        double _dt = 1,
        int seed = 1
    )
        : Process (t0)
        , r (0,1,seed)
        , state {start}
        , drift {_drift}
        , diffusion {_diffusion}
        , dt {_dt}
    {}

    double operator () (void) {
        time += dt;
        auto state_change = state * (drift*dt + diffusion*std::sqrt(dt)*r());
        auto state_new = state + state_change;
        state = std::signbit(state_new)==std::signbit(state) ? state_new : NAN;
        return state;
    }

    int GetStepNumber (void) const {return std::round(time/dt);}
};
#endif

// class SABRProcess {
// public:

//     RandomNormalDistribution r;
//     double time;
//     double F,S; // state
//     double alpha, beta, rho;
//     double dt;

//     GeometricalBrownianMotionProcess (
//         double t0 = 0,
//         double start = 1,
//         double _drift = 0,
//         double _diffusion = 1,
//         double _dt = 1,
//         int seed = 1
//     )
//         : r (0,1,seed)
//         , time (t0)
//         , state {start}
//         , drift {_drift}
//         , diffusion {_diffusion}
//         , dt {_dt}
//     {}

//     double operator () (void) {
//         time += dt;
//         auto state_change = state * (drift*dt + difS
//     int GetStepNumber (void) const {return std::round(time/dt);}
// };
