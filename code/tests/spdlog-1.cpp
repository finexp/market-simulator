#include "spdlog/spdlog.h"
#include "spdlog/cfg/helpers.h"

#include "spdlog/sinks/stdout_color_sinks.h"

#include "env.hpp"

// Examples:
//
// set global level to debug: "debug"
// turn off all logging except for logger1: "off,logger1=debug"
// turn off all logging except for logger1 and logger2: "off,logger1=debug,logger2=info"

int main (void) {
    auto log_console = spdlog::stdout_color_mt("console");

    log_console->debug("debug {}",1);
    log_console->info("info {}",1);

    spdlog::cfg::helpers::load_levels(env(
        "LOGGING",
        "console=debug",
        [&] (const std::string &result) {
            log_console->info("{}",result);
        }
    ));

    log_console->debug("debug {}",2);
    log_console->info("info {}",2);
    return 0;
}


int main3 (void) {
    auto log_console = spdlog::stdout_color_mt("console");

    auto log = [&] (const std::string &result) {
        log_console->info("{}",result);
    };

    [[maybe_unused]] auto v1 = env("aa",2.2,log);
    [[maybe_unused]] auto v2 = env("aa","a-a-a",log);
    return 0;
}

int main2 (void) {
    auto log_console = spdlog::stdout_color_mt("console");
    log_console->debug("debug {}",1);
    log_console->info("info {}",1);
    spdlog::cfg::helpers::load_levels("off,console=info");
    log_console->debug("debug {}",2);
    log_console->info("info {}",2);
    spdlog::cfg::helpers::load_levels("off,console=debug");
    log_console->debug("debug {}",3);
    log_console->info("info {}",3);
    return 0;
}

int main1 (void) {
    spdlog::cfg::helpers::load_levels("off");
    spdlog::info("Welcome to spdlog version {}.{}.{}  !", SPDLOG_VER_MAJOR, SPDLOG_VER_MINOR, SPDLOG_VER_PATCH);
    return 0;
}
