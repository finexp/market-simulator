#include "ut.hpp"

#include "fmt/core.h"
#include "fmt/ostream.h"
#include "fmt/format.h"

#include "Market.hpp"
#include "StochasticProcesses.hpp"

Market<std::string,Quote<double,double>> build_market (void) {
    using Market = Market<std::string,Quote<double,double>>;
    Market market;

    BrownianMotionProcess
        bm1 (0,0, 0,0,0.1,1),
        bm2 (0,1,-1,0,0.2,2);
    
    double tmax = 10;

    std::set<Market::Quote> quotes;
    while(bm1.GetTime()<tmax) {
        quotes.insert(Market::Quote(bm1.GetTime(),bm1.GetState()));
        bm1.Advance();
    }
    market.quotes["bm1"] = quotes;

    quotes.clear();

    while(bm2.GetTime()<tmax) {
        quotes.insert(Market::Quote(bm2.GetTime(),bm2.GetState()));
        bm2.Advance();
    }
    market.quotes["bm2"] = quotes;

    return market;
};

int main (void) {
    using namespace boost::ut;

    skip/
    "basic"_test = [] {
        expect(1==1) << "AAAA";
        expect(1==2) << "BBBB";
        expect(that% 1==1) << "CCCC";
        expect(that% 1==2) << "DDDD";
    };

    "Market_basic"_test = [] {

        using Quote = Quote<double,double>;        // types: <TimePoint,Volume>
        using Market = Market<std::string,Quote>;  // types: <ID,Quote>

        Market market;
        market
            .Add(
                "CompanyA",
                Quote(1.1, 22)
            )
            .Add(
                "CompanyC",
                Quote(1.12, -22)
            )
            .Add(
                "CompanyB",
                Quote(1.21, 122)
            )
            .Add(
                "CompanyA",
                Quote(14.1, 1222)
            )
            .Add(
                "CompanyB",
                Quote(11.1, 22)
            )
            .Add(
                "CompanyA",
                Quote(10.1, 22)
            )
            .Add(
                "CompanyA",
                Quote(2.1, 1)
            )
            .Add(
                "CompanyA",
                Quote(3.3, 44)
            );

        std::cout << market;
    };

    "build_market"_test = [&] {
        auto market = build_market();
        expect(that% int(market.quotes.size())==2);
        expect(that% market.quotes.contains("bm1"));
        expect(that% market.quotes.contains("bm2"));
        expect(that% not market.quotes.contains("bm0"));

        {
            auto q = market.GetQuote("bm1",-1);
            expect(that% not q.has_value()) << fmt::format("GetQuote() = {}",q);
        }

        {
            auto q = market.GetQuote("bm1",1);
            expect(that% q.has_value()) << fmt::format("GetQuote() = {}",q);
        }

        {
            auto q = market.GetQuote("bm0",1);
            expect(that% not q.has_value()) << fmt::format("GetQuote() = {}",q);
        }
        
        if(auto q = market.GetQuote("bm1",1); q){
            fmt::print("found: {}\n",*q);
        } else {
            fmt::print("error: {}\n",q.error());
        }

        if(auto q = market.GetQuote("bm2",2); q){
            fmt::print("found: {}\n",*q);
        } else {
            fmt::print("error: {}\n",q.error());
        }
    };
}
