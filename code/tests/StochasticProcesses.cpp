#include "ut.hpp"
using namespace boost::ut;

#include "fmt/core.h"

#include "StochasticProcesses.hpp"

int main (void) {
    "random_number_generator"_test = [] {
        int seed {1};
        std::default_random_engine engine(seed);

        double mean {0}, sigma {1};
        std::normal_distribution<> norm(mean,sigma);

        for (int n = 0; n < 10; ++n) {
            fmt::print("{} ",norm(engine));
        }
        fmt::print("\n");
    };

    "generate_random_with_function"_test = [] {

        int seed {3};
        std::default_random_engine engine(seed);

        double mean {0}, sigma {1};
        std::normal_distribution<> norm(mean,sigma);

        auto generate_next_random_value = [&] (void) {
            return norm(engine);
        };

        for (int n = 0; n < 10; ++n) {
            fmt::print("{} ",generate_next_random_value());
        }

        fmt::print("\n");
    };

    "ProcessState"_test = [] {
        std::cout << ProcessState<double>{0.1,0.2} << "\n";
    };

    "bm_process_no_drift"_test = [] {

        BrownianMotionProcess bm;

        fmt::print("bm_process_no_drift: ");

        for (int n = 0; n < 10; ++n) {
            fmt::print("{}",bm.Advance());
        }

        fmt::print("\n");
    };

    "bm_process_with_drift"_test = [] {

        BrownianMotionProcess bm(0,-1,1,1,3);

        fmt::print("bm_process_with_drift: ");

        for (int n = 0; n < 10; ++n) {
            fmt::print("{}",bm.Advance());
        }

        fmt::print("\n");
    };

    "bm_process_without_diffusion"_test = [] {

        BrownianMotionProcess bm(0,0,-1,0,1,1);

        fmt::print("bm_process_without_diffusion: ");

        for (int n = 0; n < 10; ++n) {
            fmt::print("{}",bm.Advance());
        }

        fmt::print("\n");
    };

    "gbm_process"_test = [] {

        GeometricalBrownianMotionProcess gbm(0,1,0,1,0.1,1);

        fmt::print("gbm_process: ");

        for (int n = 0; n < 10; ++n) {
            fmt::print("{}",gbm.Advance());
        }

        fmt::print("\n");
    };
}