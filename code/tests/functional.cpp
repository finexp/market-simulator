#include <variant>
#include <optional>
#include "include/ut.hpp"

#include "Version.hpp"

using type1 = std::optional<int>;

type1 f1(type1 a,type1 b){
    if(a.has_value() and b.has_value()){
        auto r = a.value()+b.value();
    if(r>=0) return r;
    }
    return {};
}

template <typename T>
std::ostream & operator << (std::ostream &os, const std::optional<T> &v){
    if( v.has_value() )
        os << v.value(); 
    else
        os << "(none)";
    return os;
}

// yes, stop() is not used!
auto invoke2(auto arg,auto shall_stop,auto f1) {
    return f1(arg);
}

auto invoke2(auto arg,auto shall_stop,auto f1,auto... f2) {
    if(auto r1 = f1(arg); shall_stop(r1))
        return r1;
    else
        return invoke2(arg,shall_stop,f2...);
}

int main (void) {
    using namespace boost::ut;

    "0"_test = [] {
        auto r = f1(1,2);
        std::cout << r << "\n";
        std::cout << f1(1,-1) << "\n";
        std::cout << f1(2,-1) << "\n";
        std::cout << f1(1,-2) << "\n";
    };
    
    "2"_test = [] {
        auto is_good = [] (std::string s) {return s[0]=='y';};
        auto f1 = [] (int x) {return x==1 ? "yes-1":"no-1";};
        auto f2 = [] (int x) {return x==2 ? "yes-2":"no-2";};
        auto f3 = [] (int x) {return x==3 ? "yes-3":"no-3";};
        auto f4 = [] (int x) {return x==4 ? "yes-4":"no-4";};
        auto f5 = [] (int x) {return x==5 ? "yes-5":"no-5";};
        auto r = invoke2(4,is_good,f1,f2,f3,f4,f5);
        std::cout << "Result2 is: " << r << "\n";
    };

    "4"_test = [] {
        using RetType = std::variant<int,std::string,std::monostate>;
        auto shall_stop = [] (const RetType& r) {
            // stop if:
            // - the results is accepted (index==0)
            // - an exception is thrown
            return r.index() != 2u;
        };
        auto f1 = [] (const Version &v) -> RetType {
            if(v.name!="f1") return std::monostate{};
            return v.number;
        };
        auto f2 = [] (const Version &v) -> RetType {
            if(v.name!="f2") return std::monostate{};
            throw std::runtime_error("f2() bug");
        };

        auto r1 = invoke2(Version{"f1",11},shall_stop,f1,f2);
        expect(that% r1.index()==0u);
        expect(that% std::get<0>(r1)==11);
    };

    should("also work with exceptions") = [] {
        using RetType = std::variant<int,std::exception,std::monostate>;
        auto shall_stop = [] (const RetType& r) {
            return r.index() != 2u;
        };
        auto f1 = [] (const Version &v) -> RetType {
            try {
                if(v.name!="f1") return std::monostate{};
                return v.number;
            } catch (const std::exception &e) {
                return e;
            }
        };
        auto f2 = [] (const Version &v) -> RetType {
            try {
                if(v.name!="f2") return std::monostate{};
                throw std::runtime_error("f2() bug");
            } catch (const std::exception &e) {
                return e;
            }
        };
        auto f3 = [] (const Version &v) -> RetType {
            try {
                if(v.name!="f3") return std::monostate{};
                return v.number;
            } catch (const std::exception &e) {
                return e;
            }
        };

        enum Calculation {Result=0u,Exception=1u,Undecided=2u};

        auto r1 = invoke2(Version{"f1",11},shall_stop,f1,f2);
        expect(that% r1.index()==Calculation::Result);
        expect(that% std::get<Calculation::Result>(r1)==11);

        auto r2 = invoke2(Version{"f2",22},shall_stop,f1,f2);
        expect(that% r2.index()==Calculation::Exception);

        auto r3 = invoke2(Version{"f3",33},shall_stop,f1,f2);
        expect(that% r3.index()==Calculation::Undecided);

        auto r4 = invoke2(Version{"f3",33},shall_stop,f1,f3);
        expect(that% r4.index()==Calculation::Result);
        expect(that% std::get<Calculation::Result>(r4)==33);
    };

    should("work with optional<variant<int,exception>>") = [] {
        using RetType = std::optional<std::variant<int,std::exception>>;
        auto shall_stop = [] (const RetType& r) {
            return r.has_value();
        };
        auto f1 = [] (const Version &v) -> RetType {
            try {
                if(v.name!="f1") return {};
                return v.number;
            } catch (const std::exception &e) {
                return e;
            }
        };
        auto f2 = [] (const Version &v) -> RetType {
            try {
                if(v.name!="f2") return {};
                throw std::runtime_error("f2() bug");
            } catch (const std::exception &e) {
                return e;
            }
        };
        auto f3 = [] (const Version &v) -> RetType {
            try {
                if(v.name!="f3") return {};
                return v.number;
            } catch (const std::exception &e) {
                return e;
            }
        };

        auto r1 = invoke2(Version{"f1",11},shall_stop,f1,f2);
        expect(that% r1.has_value());
        expect(that% r1.value().index()==0u);
        expect(that% std::get<0>(r1.value())==11);

        auto r2 = invoke2(Version{"f2",22},shall_stop,f1,f2);
        expect(that% r2.has_value());
        expect(that% r2.value().index()==1u);

        auto r3 = invoke2(Version{"f3",33},shall_stop,f1,f2);
        expect(that% r3.has_value()==false);

        auto r4 = invoke2(Version{"f3",33},shall_stop,f1,f3);
        expect(that% r4.has_value());
        expect(that% std::get<0>(r4.value())==33);
    };

    return 0;
}
