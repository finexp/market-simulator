#include "include/ut.hpp"
using namespace boost::ut;

#include <chrono>

#include "Quote.hpp"
#include "include/fmt/chrono.h"

template <typename Q>
void validate(const Q &q) {
    std::cout << q << "\n";
    auto qstr = fmt::format("{}",q);
    expect(that% qstr.find("Quote")!=std::string::npos);
    expect(that% q==q);
}

// template <typename Q>
// void validate(const Q &q1, const Q &q2) {
//     int n {0};
//     n += q1<q2 ? 1:0;
//     n += q2<q1 ? 1:0;
//     n += q1==q2 ? 1:0;
//     expect(that% n==1);
// }

int main (void) {
    "Quote_int_int"_test = [] {
        using Quote = Quote<int,int>;
        auto q = Quote(11, 22);
        validate(q);
    };

    "Quote_double_double"_test = [] {
        using Quote = Quote<double,double>;
        auto q = Quote(1.1, 22);
        validate(q);
    };

    "Quote_chrono_double"_test = [] {
        using TimePoint = std::chrono::time_point<std::chrono::system_clock>;
        using Quote = Quote<TimePoint,double>;
        auto q = Quote(std::chrono::system_clock::now(), 22);
        validate(q);
    };
}
