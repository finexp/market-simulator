#include <string>
#include <iostream>

#include "md4c/md4c-html.h"

// int md_html(const MD_CHAR* input, MD_SIZE input_size,
//             void (*process_output)(const MD_CHAR*, MD_SIZE, void*),
//             void* userdata, unsigned parser_flags, unsigned renderer_flags);

std::string md_to_html(const std::string &md) {
    std::string html;
    md_html(
        md.c_str(),
        md.length(),
        [] (const MD_CHAR* in, MD_SIZE size, void* data) {
            std::string &out = *reinterpret_cast<std::string*>(data);
            out += std::string(in,size);
        },
        &html,  // userdata
        0, // parser_flags
        0 // renderer_flags
    );
    return html;
}


int main (void) {

    std::string output_html;
    std::string input_md = R"(
# The title
## The subtitle

text text!
)";

    md_html(
        input_md.c_str(),
        input_md.length(),
        [] (const MD_CHAR* str, MD_SIZE size, void* data) {
            std::string &output_html = *reinterpret_cast<std::string*>(data);
            output_html += std::string(str,size);
        },
        &output_html,  // userdata
        0, // parser_flags
        0 // renderer_flags
    );

    std::cout << output_html << "\n";
    std::cout << md_to_html(input_md) << "\n";

    return 0;
}
