#include <iostream>
#include "fmt/core.h"
#include "fmt/ostream.h"
#include "fmt/format.h"

template <typename T>
class Base {
public:
    virtual ~Base (void) {}
    Base (const T _x) : x(_x) {}
    T x;
};

class Derived: public Base<double> {
public:
    Derived (double x) : Base(x) {}
};

// template <> template <typename T> struct fmt::formatter<Base<T>> {
//     constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
//         auto it = ctx.begin(), end = ctx.end();
//         if (it != end && *it != '}') throw format_error("invalid format");
//         return it;
//     }

//     template <typename FormatContext>
//     auto format(const Base<T>& p, FormatContext& ctx) -> decltype(ctx.out()) {
//         return format_to(ctx.out(),"[Base::format_to] {}",p.x);
//     }
// };

template <> struct fmt::formatter<Derived> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end && *it != '}') throw format_error("invalid format");
        return it;
    }

    template <typename FormatContext>
    auto format(const Derived& p, FormatContext& ctx) -> decltype(ctx.out()) {
        return format_to(ctx.out(),"[Derived::format_to] {}",p.x);
    }
};

std::ostream & operator << (std::ostream &os,const Derived &p) {
    os << fmt::format("{}",p);
    return os;
}

int main (void) {
    // std::cout << fmt::format("{}",Base(0.1)) << "\n";
    std::cout << fmt::format("{}",Derived(0.2)) << "\n";
    std::cout << Derived(0.2) << "\n";
    fmt::print("{}\n",Derived(0.2));
    return 0;
}
