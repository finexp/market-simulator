#pragma once

#include "log.hpp"
#include "include/json.hpp"
using json = nlohmann::json;

#include "Version.hpp"
#include "binding/json/all.hpp"

struct RequestDeleteMarket {
    Version version {"RequestDeleteMarket", 0};
    std::string marketId;
    bool operator == (const RequestDeleteMarket &v) const = default;
    bool operator != (const RequestDeleteMarket &v) const = default;
};

inline
void from_json(const json& j, RequestDeleteMarket& o) try {
    j.at("version")
        .get_to(o.version);
    j.at("marketId")
        .get_to(o.marketId);
} catch (const std::exception &e) {
    throw_exception("func={} {}",__func__,e.what());
}
