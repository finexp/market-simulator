#pragma once

#include <ostream>
#include "include/fmt/core.h"
#include "include/fmt/ostream.h"

template <typename T, typename V>
class Quote {
public:
    using TimePoint = T;
    using Volume = V;

    T time_point;
    V volume;
    
    Quote (TimePoint t, Volume v) : time_point (t), volume (v) {}
    
    // TODO: this is a special comparison (for Market) which shall not be done here
    bool operator < (const Quote &q) const {return time_point<q.time_point;}
    bool operator == (const Quote &q) const = default;
    bool operator != (const Quote &q) const = default;
};

template <typename T, typename V> struct fmt::formatter<Quote<T,V>> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end && *it != '}') throw format_error("invalid format");
        return it;
    }

    template <typename FormatContext>
    auto format(const Quote<T,V>& q, FormatContext& ctx) -> decltype(ctx.out()) {
        return format_to(ctx.out(),"Quote(time={},volume={})",q.time_point,q.volume);
    }
};

template <typename T, typename V>
std::ostream & operator << (std::ostream &os,const Quote<T,V> &q) {
    os << fmt::format("{}",q);
    return os;
}
