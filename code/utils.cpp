#include "utils.hpp"
#include "include/fmt/core.h"

std::string markdown_to_html(const std::string &md) {
    std::string html;
    md_html(
        md.c_str(),
        md.length(),
        [] (const MD_CHAR* in, MD_SIZE size, void* data) {
            std::string &out = *reinterpret_cast<std::string*>(data);
            out += std::string(in,size);
        },
        &html,  // userdata
        0, // parser_flags
        0 // renderer_flags
    );
    return html;
}

std::string create_html_page (
    const std::string &body,
    const std::string &style
) {
    constexpr auto page = R"(
<!DOCTYPE html>
<html>
<head>
<style>{}</style>
</head>
<body>{}</body>
</html>
)";
    return fmt::format(page,style,body);
};
