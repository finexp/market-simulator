#pragma once

#include <map>
#include <set>

#include "expected.hpp"

#include "Quote.hpp"

template <typename I, typename Q>
class Market {
public:
    using ID        = I;
    using Quote     = Q;
    using TimePoint = typename Quote::TimePoint;
    using Volume    = typename Quote::Volume;
    std::map <ID, std::set<Quote>> quotes;
    std::map <ID, std::string> quotesGeneratorInfo;
    Market & Add (ID id, Quote q) {quotes[id].insert(q); return *this;}

    // Get quote, closest to point tp, but not after it
    tl::expected <Quote,std::string> GetQuote(
        ID id,
        TimePoint tp
    ) const {
        auto it = quotes.find(id);
        if(it==quotes.end())
            return tl::make_unexpected(fmt::format("market has no quotes for \"{}\"",id));
        auto q = it->second.upper_bound(Q(tp,NAN));
        if(q==it->second.begin())
            return tl::make_unexpected(fmt::format(
                "market has {} quotes for \"{}\", but nothing before-or-at TimePoint={}",
                it->second.size(), id, tp
            ));
        return *(--q);
    }
};

template <typename L,typename R> struct fmt::formatter<typename tl::expected<L,R>> {
    constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
        auto it = ctx.begin(), end = ctx.end();
        if (it != end && *it != '}') throw format_error("invalid format");
        return it;
    }

    template <typename FormatContext>
    auto format(const tl::expected<L,R>& e, FormatContext& ctx) -> decltype(ctx.out()) {
        if(e)
            return format_to(ctx.out(),"{}",e.value());
        else
            return format_to(ctx.out(),"{}",e.error());
    }
};

template <typename T, typename V, typename ID>
std::ostream & operator << (std::ostream &os,const Market<ID,Quote<T,V>> &m) {
    fmt::print(os,"market\n");
    for(auto &[id,tv]: m.quotes){
        fmt::print(os,"{}:",id);
        for(auto &item: tv)
            fmt::print(os," ({},{})",item.time_point,item.volume);
        fmt::print(os,"\n");
    }
    return os;
}

template <typename T, typename V>
std::ostream & operator << (std::ostream &os, const tl::expected<Quote<T,V>,std::string> &q) {
    if(q.has_value())
        os << *q;
    else
        os << q.error();
    return os;
}
