#include "fmt/chrono.h"
#include "all.hpp"
#include "Version.hpp"
#include "QuotesGenerator.hpp"
// #include "RequestAddMarket.hpp"

// =========================================================

void to_json(json& j, const date::year_month_day& o) try {
    throw_exception("func={}",__func__);
} catch (const std::exception &e) {
    throw;
}

void from_json(const json& j, date::year_month_day& o) try {
    throw_exception("func={}",__func__);
} catch (const std::exception &e) {
    throw;
}

// =========================================================

void to_json(json& j, const std::chrono::time_point<std::chrono::system_clock,std::chrono::seconds>& o) try {
    j = fmt::format("{:%F %T %z}",o);
} catch (const std::exception &e) {
    throw_exception("func={} {}",__func__,e.what());
}

void from_json(const json& j, std::chrono::time_point<std::chrono::system_clock,std::chrono::seconds>& o) try {
    std::istringstream{j.get<std::string>()} >> date::parse("%F %T %z",o);
} catch (const std::exception &e) {
    throw_exception("func={} {}",__func__,e.what());
}

void to_json2(json& j, const std::chrono::time_point<std::chrono::system_clock,std::chrono::seconds>& o) {
    to_json(j,o);
}

void from_json2(const json& j, std::chrono::time_point<std::chrono::system_clock,std::chrono::seconds>& o) {
    from_json(j,o);
}

// =========================================================

void to_json(json& j, const Version& o) try {
    j["name"]   = o.name;
    j["number"] = o.number;
} catch (const std::exception &e) {
    throw_exception("func={} {}",__func__,e.what());
}

void from_json(const json& j, Version& o) try {
    j.at("name").get_to(o.name);
    j.at("number").get_to(o.number);
} catch (const std::exception &e) {
    rethrow_current_exception("func = {}",__func__);
    // throw std::runtime_error(fmt::format("func={} {}",__func__,e.what()));
}

// =========================================================

using TimePoint = std::chrono::time_point
        < std::chrono::system_clock, std::chrono::seconds >;

template <>
void to_json(json& j, const QuotesGenerator<TimePoint>& o) try {
    j["version"]   = o.version;
    j["marketId"]  = o.marketId;
    j["quotesId"]   = o.quotesId;
    j["timeStart"] = o.timeStart;
    j["timeEnd"]   = o.timeEnd;
    j["timeStep"]   = o.timeStep;
    j["calendarConversion"] = o.calendarConversion;
    to_json(j["calendarTimeStart"],o.calendarTimeStart);
} catch (const std::exception &e) {
    throw_exception("func={} {}",__func__,e.what());
}

template <>
void from_json(const json& j, QuotesGenerator<TimePoint>& o) try {
    j.at("version")
        .get_to(o.version);
    j.at("marketId")
        .get_to(o.marketId);
    j.at("quotesId")
        .get_to(o.quotesId);
    j.at("timeStart")
        .get_to(o.timeStart);
    j.at("timeEnd")
        .get_to(o.timeEnd);
    j.at("timeStep")
        .get_to(o.timeStep);
    j.at("calendarConversion")
        .get_to(o.calendarConversion);
    from_json(j.at("calendarTimeStart"),o.calendarTimeStart);
} catch (const std::exception &e) {
    throw_exception("func={} {}",__func__,e.what());
}

// =========================================================

template <>
void to_json(json& j, const SimpleBrownianMotionQuotesGenerator<TimePoint>& o) try {
    to_json(j,static_cast<SimpleBrownianMotionQuotesGenerator<TimePoint>::Base>(o));
    j["drift"]      = o.drift;
    j["diffusion"]  = o.diffusion;
    j["stateStart"] = o.startState;
} catch (const std::exception &e) {
    throw_exception("func={} {}",__func__,e.what());
}

template <>
void from_json(const json& j, SimpleBrownianMotionQuotesGenerator<TimePoint>& o) try {
    from_json(j,static_cast<SimpleBrownianMotionQuotesGenerator<TimePoint>::Base&>(o));
    j.at("drift")
        .get_to(o.drift);
    j.at("diffusion")
        .get_to(o.diffusion);
    j.at("stateStart")
        .get_to(o.startState);
} catch (const std::exception &e) {
    throw_exception("func={} {}",__func__,e.what());
}

// =========================================================
