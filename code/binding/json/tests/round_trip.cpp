#include <sstream>
#include <iostream>
#include "ut.hpp"
#include "env.hpp"

#include "all.hpp"
#include "Version.hpp"
#include "QuotesGenerator.hpp"

using namespace boost::ut;

template <typename T>
void run(const T &obj1){
    json j = obj1;
    expect(that% j.is_object()) << j;
    T obj2 = j;
    expect(that% obj1 == obj2);
}

// =========================================================

using TimePoint =
    std::chrono::time_point <
        std::chrono::system_clock,
        std::chrono::seconds
    >;

// =========================================================

int main(){

    spdlog::cfg::helpers::load_levels(env(
        "LOGGING",
        "debug", // "off,loggername=debug"
        [&] (const std::string &result) {
            std::cout << result << "\n";
        }
    ));

    "Version_json"_test = [] {
        run(Version {});
        run(Version {"my type",11});
    };

    "GenerateQuotes_json"_test = [] {
        auto g =
            QuotesGenerator<
                std::chrono::time_point<
                    std::chrono::system_clock,
                    std::chrono::seconds>>
            ();
        g.marketId = "m1";
        g.quotesId = "q1";
        g.timeStart = 1.1;
        g.timeEnd = 2.2;
        run(g);
        fmt::print("{}\n",json{g}.dump(2));
    };
}
