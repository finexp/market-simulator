#include "ut.hpp"
using namespace boost::ut;

#include "date/date.h"
#include <iostream>
#include <sstream>

#include "fmt/chrono.h"

#include "env.hpp"
#include "log.hpp"

using namespace std;
using namespace date;

using time_point =
    std::chrono::time_point
        < std::chrono::system_clock, std::chrono::seconds >;

int main () {

    spdlog::cfg::helpers::load_levels(env(
        "LOGGING",
        "debug", // "off,loggername=debug"
        [&] (const std::string &result) {
            std::cout << result << "\n";
        }
    ));

    "0"_test = [] {
        istringstream in{"Thu, 9 Jan 2014 12:35:34 +0000"};
        sys_seconds tp;
        in >> parse("%a, %d %b %Y %T %z", tp);
        info("{}",tp);
    };

    "date-time type"_test = [] {
        auto tp = sys_days {jan/3/1970} + 7h + 33min + 20s;
        expect(that% tp.time_since_epoch() == 200000s);
        info("time point: {}",tp);
        auto dp = floor<days>(tp);
        info("day point : {}",tp);
        auto s = tp-dp;
        expect(that% s == 27200s);
        auto time = make_time(s);
        expect(that% time.hours() == 7h);
        expect(that% time.minutes() == 33min);
        expect(that% time.seconds() == 20s);
        auto ymd = year_month_day{dp};
        expect(that% ymd.year() == 1970_y);
        expect(that% ymd.month() == jan);
        expect(that% ymd.day() == 3_d);
    };

    "date-time now"_test = [] {
        auto tp = floor<std::chrono::seconds>(chrono::system_clock::now());
        info("time point: {}",tp);
    };

    "weekday"_test = [] {
        expect(that% weekday{2001_y/jul/4} == wed);
    };

    "output"_test = [] {
        auto tp = sys_days {2022_y/jul/11} + 22h + 37min + 11s;
        info("{:%Y-%m-%d %T}",tp);
    };
}
