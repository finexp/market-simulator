#pragma once

#include "date/date.h"

#include "log.hpp"
#include "include/json.hpp"
using json = nlohmann::json;

struct Version;
void to_json(json& j, const Version& o);
void from_json(const json& j, Version& o);

void to_json(json& j, const date::year_month_day& o);
void from_json(const json& j, date::year_month_day& o);

void to_json(
    json&,
    const std::chrono::time_point<std::chrono::system_clock,std::chrono::seconds>&
);
void from_json(
    const json&,
    std::chrono::time_point<std::chrono::system_clock,std::chrono::seconds>&
);

void to_json2(
    json&,
    const std::chrono::time_point<std::chrono::system_clock,std::chrono::seconds>&
);
void from_json2(
    const json&,
    std::chrono::time_point<std::chrono::system_clock,std::chrono::seconds>&
);


template <typename>
struct QuotesGenerator;
template <typename T>
void to_json(json& j, const QuotesGenerator<T>& o);
template <typename T>
void from_json(const json& j, QuotesGenerator<T>& o);

template <typename>
struct SimpleBrownianMotionQuotesGenerator;
template <typename T>
void to_json(json& j, const SimpleBrownianMotionQuotesGenerator<T>& o);
template <typename T>
void from_json(const json& j, SimpleBrownianMotionQuotesGenerator<T>& o);

struct RequestAddMarket;
void to_json(json& j, const RequestAddMarket& o);
void from_json(const json& j, RequestAddMarket& o);
