#pragma once

// https://google.github.io/styleguide/jsoncstyleguide.xml#error.code

#include <vector>
#include <string>
#include "include/json.hpp"
using namespace nlohmann;

class JsonErrorInfo {
public:
    json body;
    JsonErrorInfo(void) = default;
    template <typename T>
    JsonErrorInfo(T && obj) : body {obj} {}
    template <typename T>
    JsonErrorInfo & Set(const std::string &key,T && value) {body[key]=value; return *this;}
    // JsonErrorInfo & SetDomain(const std::string &s) {return Set("domain",s);}
    // JsonErrorInfo & SetReason(const std::string &s) {return Set("reason",s);}
    JsonErrorInfo & SetMessage(const std::string &s) {return Set("message",s);}
    // JsonErrorInfo & SetLocation(const std::string &s) {return Set("location",s);}
    operator json (void) const {
        return body;
    }
    operator json & (void) {
        return body;
    }
};

class JsonErrorReply {
public:
    json body;
    JsonErrorReply & SetCode(int code) {body["code"]=code; return *this;}
    JsonErrorReply & SetMessage(const std::string &message) {body["message"]=message; return *this;}
    
    JsonErrorReply & SetErrors(const std::vector<std::string> &messages) {
        json errors;
        for(auto &s:messages){
            JsonErrorInfo e;
            e.SetMessage(s);
            errors.push_back(s);
        }
        body["errors"] = std::move(errors);
        return *this;
    }

    operator json (void) const {
        return {{"error",body}};
    }
};
