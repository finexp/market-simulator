#pragma once

#include <stdio.h>
#include <string>
#include <optional>
#include "fmt/core.h"

template<typename T>
T env_convert(const char *v);

template<>
const char* env_convert<const char*>(const char *v) {
    return v;
}

template<>
int env_convert<int>(const char *v) {
    return std::atoi(v);
}

template<>
float env_convert<float>(const char *v) {
    return std::atof(v);
}

template<>
double env_convert<double>(const char *v) {
    return std::atof(v);
}

// Get T value either from the environment variable
// or from the default value (if it is not set)
// Call function f()
template<typename T, typename F>
T env(const char *name,T defval,F logging) {

    std::optional<T> rv;
    std::string src;
    const char *s = getenv(name);

    if (s) {
        src = "environment";
        rv = env_convert<T>(s);
    } else {
        src = "default";
        rv = defval;
    }

    logging(fmt::format("{} [{}] {}",name,src,rv.value()));

    return rv.value();
}

template<typename T>
T env(const char *name,const T defval) {
    const char *s = getenv(name);
    return s ? env_convert<T>(s) : defval;
}
