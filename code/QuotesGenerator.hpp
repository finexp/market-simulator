#pragma once

#include <chrono>
#include <set>
#include "Version.hpp"
#include "Quote.hpp"

struct RandomNumbersGenerator {
    int seed {0};
};

// single market, single quote
template <typename T>
struct QuotesGenerator {
    using TimePoint = T;
    Version version {"QuotesGenerator", 0};
    std::string marketId;
    std::string quotesId;
    std::string genQuotes; // method of quotes generation (SBM, GBM, HW)
    std::string calendarConversion;
    TimePoint calendarTimeStart;

    long double timeStart, timeEnd, timeStep;

    bool operator == (const QuotesGenerator &v) const = default;
    bool operator != (const QuotesGenerator &v) const = default;
};

template <typename T>
std::ostream & operator << (std::ostream &os, const QuotesGenerator<T> &o) {
    os << fmt::format(
        "{} marketId='{}' quotesId='{}' \n",
        o.version,
        o.marketId,
        o.quotesId
    );
    return os;
}

// template <typename T=std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds>>
template <typename T>
struct SimpleBrownianMotionQuotesGenerator: QuotesGenerator<T> {
    
    using Base = QuotesGenerator<T>;
    float startState, drift, diffusion;

    RandomNumbersGenerator rng;

    bool operator == (const SimpleBrownianMotionQuotesGenerator &v) const = default;
    bool operator != (const SimpleBrownianMotionQuotesGenerator &v) const = default;
};

template <typename Quote>
std::set<Quote> GenerateQuotes(const SimpleBrownianMotionQuotesGenerator<typename Quote::TimePoint> &qg);

class Session;
template <typename T>
void AddQuotesToSession(Session &, const SimpleBrownianMotionQuotesGenerator<T> &generator);
