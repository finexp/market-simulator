#include "Session.hpp"
#include "log.hpp"
#include "StochasticProcesses.hpp"
#include "QuotesGenerator.hpp"

#include <chrono>
using namespace std::chrono;

void Session::AddQuotes(
    const Session::_ID& marketId,
    const Session::_ID& quotesId,
    Session::_Quotes && quotes
){
    auto &market = GetMarket(marketId);
    auto quotes_it=market.quotes.find(quotesId);
    if(quotes_it!=market.quotes.end())
        throw_exception("{} market {} already has {} quotes {}",__func__,marketId,quotesId,quotes_it->second.size());
    debug("Moving {} quotes to {}:{}",quotes.size(),marketId,quotesId);
    market.quotes.emplace(quotesId,quotes);
}

void Session::AddQuotesGeneratorInfo(const _ID& marketId, const _ID& quotesId,std::string && text){
    auto &market = GetMarket(marketId);
    auto qit=market.quotesGeneratorInfo.find(quotesId);
    if(qit!=market.quotesGeneratorInfo.end())
        throw_exception("{} market {} already has {} quotesGeneratorInfo",__func__,marketId,quotesId);
    market.quotesGeneratorInfo.emplace(quotesId,text);
}

void Session::AddMarket(const _ID& marketId){
    if(const auto it=markets.find(marketId); it==markets.end()){
        markets[marketId] = Session::_Market {};
    } else
        throw_exception("{} marketId already exists: {}",__func__,marketId);
}

void Session::DeleteMarket(const _ID& marketId){
    if(const auto it=markets.find(marketId); it!=markets.end()){
        markets.erase(marketId);
    } else
        throw_exception("{} marketId does not exist: {}",__func__,marketId);
}

// template<>
// void Session::AddQuotes(const SimpleBrownianMotionQuotesGenerator<Session::_Time> &generator){
//     AddQuotes(
//         qg.marketId,
//         qg.quotesId,
//         GenerateQuotes<Session::_Quote>(qg)
//     );
// }

#if 0
void Session::AddQuotes(const SimpleBrownianMotionQuotesGenerator &qg) {
    debug("{}",__func__);

    auto market_it = markets.find(qg.marketId);
    if(market_it==markets.end())
        throw_exception("{} market not found: {}",__func__,qg.marketId);
    auto &market = market_it->second;
    debug("market {} has {} quotes",market_it->first,market.quotes.size());
    auto quotes_it=market.quotes.find(qg.quoteId);
    if(quotes_it!=market.quotes.end())
        throw_exception("{} market {} already has {} quotes {}",__func__,qg.marketId,qg.quoteId,quotes_it->second.size());

    std::set<Session::_Quote> quotes;

    auto process = BrownianMotionProcess(0,0, 0,1,3,1);

    auto run = [&process] (int iter) -> bool {
        return iter<1000 && process.GetTime()<100;
    };

    if(qg.calendarConversion!="")
        throw_exception("{} QuotesGenerator::calendarConversion must be empty now.",__func__);

    auto toTime = [&qg] (double year_fraction) -> _Time {
        constexpr auto SecondsInYear = 365.25*24*60*60;
        return qg.calendarTimeStart + std::chrono::seconds{int(year_fraction*SecondsInYear)};
    };

    for(int iter=0; run(iter); iter++,process.Advance()){
        info("AddQuotes: {}",process());
        quotes.emplace(toTime(process.GetTime()),process.GetState());
    }
    
    debug("{} {} {} quotes",qg.marketId,qg.quoteId,quotes.size());


    market.quotes.emplace(std::move(qg.quoteId),std::move(quotes));

    // BrownianMotionProcess (t0,0, 0,1,3,1), // t0,W0, drift, diffusion, dt, seed
    // const int quotes_max = 100;
    // for(int i=0; i<quotes_max; i++){
    //     process.Advance();
    //     if(process.GetTime()>tmax)
    //         break;
    //     info("AddQuotes: {}",process());
    //     quotes.emplace(toTime(process.GetTime()),process.GetState());
    // }

    // market.quotes.emplace(std::move(quote_name),std::move(quotes));
}
#endif


#if 0
void Session::AddQuotes(const QuotesGenerator <Session::_Time> &qg) {
    debug("{}",__func__);

    auto market_it = markets.find(qg.marketId);
    if(market_it==markets.end())
        throw_exception("{} market not found: {}",__func__,qg.marketId);
    auto &market = market_it->second;
    debug("market {} has {} quotes",market_it->first,market.quotes.size());
    auto quotes_it=market.quotes.find(qg.quoteId);
    if(quotes_it!=market.quotes.end())
        throw_exception("{} market {} already has {} quotes {}",__func__,qg.marketId,qg.quoteId,quotes_it->second.size());

    std::set<Session::_Quote> quotes;

    auto process = BrownianMotionProcess(0,0, 0,1,3,1);

    auto run = [&process] (int iter) -> bool {
        return iter<1000 && process.GetTime()<100;
    };

    if(qg.calendarConversion!="")
        throw_exception("{} QuotesGenerator::calendarConversion must be empty now.",__func__);

    auto toTime = [&qg] (double year_fraction) -> _Time {
        constexpr auto SecondsInYear = 365.25*24*60*60;
        return qg.calendarTimeStart + std::chrono::seconds{int(year_fraction*SecondsInYear)};
    };

    for(int iter=0; run(iter); iter++,process.Advance()){
        info("AddQuotes: {}",process());
        quotes.emplace(toTime(process.GetTime()),process.GetState());
    }
    
    debug("{} {} {} quotes",qg.marketId,qg.quoteId,quotes.size());


    market.quotes.emplace(std::move(qg.quoteId),std::move(quotes));

    // BrownianMotionProcess (t0,0, 0,1,3,1), // t0,W0, drift, diffusion, dt, seed
    // const int quotes_max = 100;
    // for(int i=0; i<quotes_max; i++){
    //     process.Advance();
    //     if(process.GetTime()>tmax)
    //         break;
    //     info("AddQuotes: {}",process());
    //     quotes.emplace(toTime(process.GetTime()),process.GetState());
    // }

    // market.quotes.emplace(std::move(quote_name),std::move(quotes));
}
#endif

// void AddQuotes(
//     Market<std::string,Quote<Session::TimePoint,double>> &market,
//     std::string quote_name
//     // Process<double> process,
//     // double tmax
// ) {
//     market.quotes[quote_name] = {};
// }

// void AddQuotes(
//     Market<std::string,Quote<Session::TimePoint,double>> &market,
//     std::string quote_name,
//     Process<double> && process,
//     double tmax,
//     std::function<Session::TimePoint(double)> toTimePoint,
//     int quotes_max = 10
// ) {
//     if(market.quotes.contains(quote_name))
//         throw std::runtime_error(fmt::format("The market already has quote '{}'",quote_name));

//     std::set<Quote<Session::TimePoint,double>> quotes;

//     for(int i=0; i<quotes_max; i++){
//         process.Advance();
//         if(process.GetTime()>tmax)
//             break;
//         info("AddQuotes: {}",process());
//         quotes.insert(toTimePoint(process.GetState()));
//     }

//     market.quotes.emplace(std::move(quote_name),std::move(quotes));
// }

// void AddQuotes(
//     Session::_Market &market,
//     Session::_ID quote_name,
//     Process<double> && process,
//     double tmax,
//     std::function<Session::_Time(double)> toTime,
//     int quotes_max = 10
// ) {
//     if(market.quotes.contains(quote_name))
//         throw std::runtime_error(fmt::format("The market already has quote '{}'",quote_name));

//     std::set<Session::_Quote> quotes;

//     for(int i=0; i<quotes_max; i++){
//         process.Advance();
//         if(process.GetTime()>tmax)
//             break;
//         info("AddQuotes: {}",process());
//         quotes.emplace(toTime(process.GetTime()),process.GetState());
//     }

//     market.quotes.emplace(std::move(quote_name),std::move(quotes));
// }

Session::Session (void) {
    // info("{}",__func__);

    // constexpr _Time tp0 = sys_days {January/9/2014} + 12h + 35min + 34s;
    // static_assert(tp0 == _Time{1389270934s}, ""); 

    // double t0 {0}, tmax {10};

    // ::AddQuotes (
    //     markets["0"],
    //     "bm3",
    //     BrownianMotionProcess(t0,0, 0,1,3,1), // t0,W0, drift, diffusion, dt, seed
    //     tmax,
    //     [&tp0] (double year_fraction) -> _Time {
    //         constexpr auto SecondsInYear = 365.25*24*60*60;
    //         return tp0 + std::chrono::seconds{int(year_fraction*SecondsInYear)};
    //     }
    // );

}
