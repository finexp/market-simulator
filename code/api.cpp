#include <optional>
#include <variant>

#include "include/json.hpp"
using namespace nlohmann;

#include "api.hpp"
#include "Session.hpp"
#include "log.hpp"
#include "utils.hpp"

#include "Version.hpp"
#include "QuotesGenerator.hpp"
#include "RequestAddMarket.hpp"
#include "RequestDeleteMarket.hpp"

#include "binding/json/all.hpp"
#include "JsonErrorReply.hpp"

namespace api {

Session & GetSession (void) {
    static Session session;
    return session;
}

void markets_GET (const Request &req, Response &res){
    json j;
    for(const auto &[id,market]: GetSession().markets ){
        j.push_back(json{
            {"name", id},
            {"numQuoteIds", market.quotes.size()},
            {"totQuotes", std::accumulate(
                market.quotes.begin(),
                market.quotes.end(),
                0,
                [](int n,const auto &q){return n+q.second.size();})
            },
        });
    }
    res.set_content(j.dump(), "application/json");
}

void markets_id_GET (const Request &req, Response &res){
    if(req.matches.size()!=2)
        throw_exception("{}",__func__);
    const auto &marketId = req.matches[1];
    const auto &markets = GetSession().markets;
    if(const auto it=markets.find(marketId); it!=markets.end()){
        const Session::_Market &market = it->second;
        json j = {
            {"marketId", marketId},
            {"numQuoteIds", market.quotes.size()},
            {"totQuotes", std::accumulate(
                market.quotes.begin(),
                market.quotes.end(),
                0,
                [](int n,const auto &q){return n+q.second.size();}
            )}
        };
        res.set_content(j.dump(), "application/json");
    } else {
        res.status = NotFound;
    }
}

void markets_id_PUT (const Request &req, Response &res){
    if(req.matches.size()!=2)
        throw_exception("{}",__func__);
    const auto &marketId = req.matches[1];
    auto &markets = GetSession().markets;
    if(const auto it=markets.find(marketId); it==markets.end()){
        markets[marketId] = Session::_Market {};
    } else {
        res.status = Conflict;
    }
}

void markets_id_DEL (const Request &req, Response &res){
    if(req.matches.size()!=2)
        throw_exception("{}",__func__);
    const auto &marketId = req.matches[1];
    auto &markets = GetSession().markets;
    if(const auto it=markets.find(marketId); it!=markets.end()){
        markets.erase(marketId);
    } else {
        res.status = Conflict;
    }
}

void markets_id_quotes_GET (const Request &req, Response &res){
    if(req.matches.size()!=2)
        throw_exception("{}",__func__);
    const auto &marketId = req.matches[1];
    const auto &markets = GetSession().markets;
    json j;
    if(const auto it=markets.find(marketId); it!=markets.end()){
        const Session::_Market &market = it->second;
        for(const auto &[qid,quotes]: market.quotes){
            j.push_back({
                {"quoteId",qid},
                {"numQuotes",quotes.size()}
            });
        }
    }
    res.set_content(j.dump(), "application/json");
}

void markets_id_quotes_id_GET (const Request &req, Response &res){
    if(req.matches.size()!=3)
        throw_exception("{}",__func__);
    const auto &marketId = req.matches[1];
    const auto &quoteId = req.matches[2];
    const auto &markets = GetSession().markets;
    json j;
    if(const auto it=markets.find(marketId); it!=markets.end()){
        const Session::_Market &market = it->second;
        if(const auto qit=market.quotes.find(quoteId); qit!=market.quotes.end()){
            for(const auto &[time,value]: qit->second){
                json jtime;
                to_json2(jtime,time);
                j.push_back({
                    // {"timeReal",},
                    {"time",std::move(jtime)},
                    {"value",value}
                });
            }
        }
    }
    res.set_content(j.dump(), "application/json");
}

void markets_id_quotes_id_info_GET (const Request &req, Response &res){
    if(req.matches.size()!=3)
        throw_exception("{}",__func__);
    const auto &marketId = req.matches[1];
    const auto &quoteId = req.matches[2];
    const auto &markets = GetSession().markets;
    json j;
    if(const auto it=markets.find(marketId); it!=markets.end()){
        const Session::_Market &market = it->second;
        if(const auto qit=market.quotesGeneratorInfo.find(quoteId); qit!=market.quotesGeneratorInfo.end()){
            res.set_content(qit->second, "application/json");
        }
    }
}

struct AddQuotesWithSimpleBrowninanMotion {
    std::string marketId;
    std::string quotesId;
    std::string timeStart;
};

std::string EndPoint::ToHtml(void) const {
    std::string s;

    s += fmt::format("<h2 class='brief'>{}</h2>\n",brief);
    s += "<dl>\n";

    s += fmt::format("<dt>api</dt>\n");
    s += fmt::format("<dd>{}</dd>\n",path);

    s += fmt::format("<dt>method</dt>\n");
    s += fmt::format("<dd>{}</dd>\n",to_string(method));

    s += "</dl>\n";

    s += "<div class='doc-md'>\n";
    s += markdown_to_html(doc_md);
    s += "</div>";

    return s;
}

// yes, stop() is not used!
auto invoke(auto arg,auto shall_stop,auto f) {
    return f(arg);
}

auto invoke(auto arg,auto shall_stop,auto f,auto...rest) {
    if(auto r = f(arg); shall_stop(r))
        return r;
    else
        return invoke(arg,shall_stop,rest...);
}

void action_POST (const Request &req, Response &res){
    const json j = json::parse(req.body);
    
    debug("{} {}",__func__,j.dump());

    if(j.is_object()==false)
        throw_exception("{} JSON _object_ argument was expected.",__func__);

    Version version = j.at("version");

    struct ArgType {json j; Version v;};
    using RetType = std::optional<std::variant<json,std::exception_ptr>>;
    auto stop_condition = [] (const RetType& r) {
        return r.has_value();
    };

    auto handleSimpleBrownianMotionQuotesGenerator = [&stop_condition] (const ArgType &a) -> RetType {

        auto version0 = [] (const ArgType &a) -> RetType {
            if(a.v.number!=0) return {};
            SimpleBrownianMotionQuotesGenerator<Session::_Quote::TimePoint> qg = a.j;
            AddQuotesToSession(GetSession(),qg);
            return json {};
        };

        try {
            if(a.v.name!="SimpleBrownianMotionQuotesGenerator") return {};
            return invoke(a,stop_condition,version0);
        } catch (const std::exception &) {
            return std::current_exception();
        }
    };

    auto handleAddMarket = [&stop_condition] (const ArgType &a) -> RetType {
        auto version0 = [] (const ArgType &a) -> RetType {
            if(a.v.number!=0) return {};
            GetSession().AddMarket(RequestAddMarket(a.j).marketId);
            return json {};
        };

        try {
            if(a.v.name!="AddMarket") return {};
            return invoke(a,stop_condition,version0);
        } catch (const std::exception &e) {
            auto ee = get_exceptions(e);
            warning("An exception has occured: size={}: {}",ee.size(),e.what());
            for(auto item:ee)
                warning(" ... {}",item);
            return std::current_exception();
        }
    };

    auto handleDeleteMarket = [&stop_condition] (const ArgType &a) -> RetType {
        auto version0 = [] (const ArgType &a) -> RetType {
            if(a.v.number!=0) return {};
            GetSession().DeleteMarket(RequestDeleteMarket(a.j).marketId);
            return json {};
        };

        try {
            if(a.v.name!="DeleteMarket") return {};
            return invoke(a,stop_condition,version0);
        } catch (const std::exception &e) {
            auto ee = get_exceptions(e);
            warning("An exception has occured: size={}: {}",ee.size(),e.what());
            for(auto item:ee)
                warning(" ... {}",item);
            return std::current_exception();
        }
    };

    auto result = invoke(
        ArgType {j, version},
        stop_condition,
        handleSimpleBrownianMotionQuotesGenerator,
        handleAddMarket,
        handleDeleteMarket
    );

    json reply_json;
    std::string reply_type;
    if(result){
        auto &value = result.value();
        debug("{} result index is: {}",__func__,value.index());
        switch(value.index()){
            case 0: {
                reply_json = std::move(std::get<0>(value));
                reply_type = "application/json";
                res.status = OK;
                break;
            }
            case 1: {
                reply_type = "application/problem+json";
                res.status = BadRequest;
                reply_json = JsonErrorReply()
                    .SetErrors(get_exceptions(std::get<1>(value)))
                    .SetCode(res.status)
                ;
                break;
            }
            default: {
                reply_json = JsonErrorReply()
                    .SetMessage("Internal error")
                    .SetCode(res.status)
                ;
                reply_type = "application/problem+json";
                res.status = InternalServerError;
            }
        }
    } else {
        reply_type = "application/problem+json";
        res.status = BadRequest;
        reply_json = JsonErrorReply()
            .SetMessage("There is no code which can execute the request.")
            .SetCode(res.status)
        ;
    }
    
    reply_json["version"] = Version {"Reply",0};
    reply_json["replyToRequest"] = version;
    reply_json["httpCode"] = res.status;

    info("**** REPLY code={} {} ****\n{}",res.status,reply_type, reply_json.dump(4));

    res.set_content(reply_json.dump(), reply_type.c_str());
}

}
