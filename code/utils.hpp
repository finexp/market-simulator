#include <string>
#include "md4c/md4c-html.h"

std::string create_html_page (
    const std::string &body="",
    const std::string &style=""
);

std::string markdown_to_html(const std::string &md);
