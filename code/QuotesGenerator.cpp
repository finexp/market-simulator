#include "Session.hpp"
#include "QuotesGenerator.hpp"
#include "StochasticProcesses.hpp"
#include "log.hpp"
#include "binding/json/all.hpp"

using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds>;

template <>
std::set<Quote<TimePoint,double>> GenerateQuotes(
    const SimpleBrownianMotionQuotesGenerator<TimePoint> &qg
){
    debug("{}",__func__);

    std::set<Quote<TimePoint,double>> quotes;

    auto process = BrownianMotionProcess(
        qg.timeStart,
        qg.startState,
        qg.drift,
        qg.diffusion,
        qg.timeStep,
        qg.rng.seed
    );

    auto run = [&process,&qg] (int iter) -> bool {
        return iter<1000 && process.GetTime()<qg.timeEnd;
    };

    if(qg.calendarConversion!="")
        throw_exception("{} QuotesGenerator::calendarConversion must be empty now.",__func__);

    auto toTime = [&qg] (double year_fraction) -> TimePoint {
        constexpr auto SecondsInYear = 365.25*24*60*60;
        return qg.calendarTimeStart + std::chrono::seconds{int(year_fraction*SecondsInYear)};
    };

    for(int iter=0; run(iter); iter++,process.Advance()){
        info("AddQuotes: {}",process());
        quotes.emplace(toTime(process.GetTime()),process.GetState());
    }
    
    return quotes;
}

template <>
void AddQuotesToSession(Session &session, const SimpleBrownianMotionQuotesGenerator<TimePoint> &generator){
    json j = generator;
    session.AddQuotesGeneratorInfo(generator.marketId,generator.quotesId,j.dump());
    session.AddQuotes(
        generator.marketId,
        generator.quotesId,
        GenerateQuotes<Session::_Quote>(generator)
    );
}

// void Session::AddQuotes(const SimpleBrownianMotionQuotesGenerator &qg) {
//     debug("{}",__func__);

//     auto market_it = markets.find(qg.marketId);
//     if(market_it==markets.end())
//         throw_exception("{} market not found: {}",__func__,qg.marketId);
//     auto &market = market_it->second;
//     debug("market {} has {} quotes",market_it->first,market.quotes.size());
//     auto quotes_it=market.quotes.find(qg.quoteId);
//     if(quotes_it!=market.quotes.end())
//         throw_exception("{} market {} already has {} quotes {}",__func__,qg.marketId,qg.quoteId,quotes_it->second.size());

//     std::set<Session::_Quote> quotes;

//     auto process = BrownianMotionProcess(0,0, 0,1,3,1);

//     auto run = [&process] (int iter) -> bool {
//         return iter<1000 && process.GetTime()<100;
//     };

//     if(qg.calendarConversion!="")
//         throw_exception("{} QuotesGenerator::calendarConversion must be empty now.",__func__);

//     auto toTime = [&qg] (double year_fraction) -> _Time {
//         constexpr auto SecondsInYear = 365.25*24*60*60;
//         return qg.calendarTimeStart + std::chrono::seconds{int(year_fraction*SecondsInYear)};
//     };

//     for(int iter=0; run(iter); iter++,process.Advance()){
//         info("AddQuotes: {}",process());
//         quotes.emplace(toTime(process.GetTime()),process.GetState());
//     }
    
//     debug("{} {} {} quotes",qg.marketId,qg.quoteId,quotes.size());


//     market.quotes.emplace(std::move(qg.quoteId),std::move(quotes));

//     // BrownianMotionProcess (t0,0, 0,1,3,1), // t0,W0, drift, diffusion, dt, seed
//     // const int quotes_max = 100;
//     // for(int i=0; i<quotes_max; i++){
//     //     process.Advance();
//     //     if(process.GetTime()>tmax)
//     //         break;
//     //     info("AddQuotes: {}",process());
//     //     quotes.emplace(toTime(process.GetTime()),process.GetState());
//     // }

//     // market.quotes.emplace(std::move(quote_name),std::move(quotes));
// }
