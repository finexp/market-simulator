#pragma once

#include "spdlog/spdlog.h"
#include "spdlog/async.h"
#include "spdlog/cfg/helpers.h" // spdlog::cfg::helpers::load_levels

template<typename... Args>
void trace(const fmt::format_string<Args...> &str, Args&&... args) {
    SPDLOG_TRACE(str, std::forward<Args>(args)...);
}

template<typename... Args>
void debug(const fmt::format_string<Args...> &str, Args&&... args) {
    spdlog::debug(str, std::forward<Args>(args)...);
}

template<typename... Args>
void info(const fmt::format_string<Args...> &str, Args&&... args) {
    spdlog::info(str, std::forward<Args>(args)...);
}

template<typename... Args>
void warn(const fmt::format_string<Args...> &str, Args&&... args) {
    spdlog::warn(str, std::forward<Args>(args)...);
}

template<typename... Args>
void warning(const fmt::format_string<Args...> &str, Args&&... args) {
    spdlog::warn(str, std::forward<Args>(args)...);
}

template<typename... Args>
void error(const fmt::format_string<Args...> &str, Args&&... args) {
    spdlog::error(str, std::forward<Args>(args)...);
}

template<typename... Args>
void critical(const fmt::format_string<Args...> &str, Args&&... args) {
    spdlog::critical(str, std::forward<Args>(args)...);
}

template<typename... Args>
[[noreturn]]
inline
void rethrow_current_exception (
    const fmt::format_string<Args...> &str,
    Args&&... args
) {
    const std::string message = fmt::format(str, std::forward<Args>(args)...);
    if(not message.empty())
        error("{}",message);
    try {
        std::rethrow_exception(std::current_exception());
    }
    catch(const std::invalid_argument& e) {
        std::throw_with_nested(std::invalid_argument(message));
    }
    catch(const std::logic_error& e) {
        std::throw_with_nested(std::logic_error(message));
    }
    // etc - default to a runtime_error 
    catch(...) {
        std::throw_with_nested(std::runtime_error(message));
    }
}

inline
void fill_exceptions_list (
    std::vector<std::string> &exceptions,
    const std::exception& e
) {
    exceptions.push_back(e.what());
    try {
        std::rethrow_if_nested(e);
    } catch(const std::exception& nested_exception) {
        fill_exceptions_list(exceptions,nested_exception);
    } catch(...) {
        fill_exceptions_list(exceptions,std::logic_error("unknown exception"));
    }
}

inline
void fill_exceptions_list (
    std::vector<std::string> &exceptions,
    const std::exception_ptr &eptr=std::current_exception()
) {
    static auto get_nested = [](auto &e) -> std::exception_ptr {
        try { return dynamic_cast<const std::nested_exception &>(e).nested_ptr(); }
        catch (const std::bad_cast&) { return nullptr; }
    };
    try {
        if (eptr)
            std::rethrow_exception(eptr);
    } catch (const std::exception &e) {
        exceptions.push_back(e.what());
        fill_exceptions_list(exceptions,get_nested(e)); // rewind all nested exception
    }
}

inline
std::vector<std::string> get_exceptions (
    const std::exception& e
) {
    std::vector<std::string> exceptions;
    fill_exceptions_list(exceptions,e);
    return exceptions;
}

inline
std::vector<std::string> get_exceptions (
    const std::exception_ptr& e
) {
    std::vector<std::string> exceptions;
    fill_exceptions_list(exceptions,e);
    return exceptions;
}

// #include <source_location>

// inline
// auto fname(const std::source_location& location = std::source_location::current()) {
//     return location.function_name();
// }

template<typename... Args>
[[noreturn]]
inline
void throw_exception (
    const fmt::format_string<Args...> &str,
    Args&&... args
) {
    auto message = fmt::format(str, std::forward<Args>(args)...);
    if(not message.empty())
        error("{}",message);
    throw std::runtime_error(message);
}

#if 0
void logger_init (const std::string &config_json);

template<typename... Args>
[[noreturn]]
inline
void throw_exception (
    const fmt::format_string<Args...> &str,
    Args&&... args
) {
    auto message = fmt::format(str, std::forward<Args>(args)...);
    if(not message.empty())
        error("{}",message);
    throw std::runtime_error(message);
}

inline
void fill_exceptions_list (
    std::vector<std::string> &exceptions,
    const std::exception& e
) {
    exceptions.push_back(e.what());
    try {
        std::rethrow_if_nested(e);
    } catch(const std::exception& nested_exception) {
        fill_exceptions_list(exceptions,nested_exception);
    } catch(...) {
        fill_exceptions_list(exceptions,std::logic_error("unknown exception"));
    }
}

inline
std::vector<std::string> get_exceptions (
    const std::exception& e
) {
    std::vector<std::string> exceptions;
    fill_exceptions_list(exceptions,e);
    return exceptions;
}

template<typename... Args>
[[noreturn]]
inline
void rethrow_current_exception (
    const fmt::format_string<Args...> &str,
    Args&&... args
) {
    const std::string message = fmt::format(str, std::forward<Args>(args)...);
    if(not message.empty())
        error("{}",message);
    try {
        std::rethrow_exception(std::current_exception());
    }
    catch(const std::invalid_argument& e) {
        std::throw_with_nested(std::invalid_argument(message));
    }
    catch(const std::logic_error& e) {
        std::throw_with_nested(std::logic_error(message));
    }
    // etc - default to a runtime_error 
    catch(...) {
        std::throw_with_nested(std::runtime_error(message));
    }
}

#ifndef __CUDACC__

#include <source_location>
#include "io.hpp"

inline
auto fname(const std::source_location& location = std::source_location::current()) {
    return location.function_name();
}

[[noreturn]]
inline
void
rethrow_current_exception(
    const std::source_location location = std::source_location::current()
){
    rethrow_current_exception("{}",location.function_name());
}

#endif

#endif