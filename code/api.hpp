#pragma once

#include "log.hpp"
#include "httplib.h"
using namespace httplib;

enum Method {DEL, GET, PATCH, POST, PUT};

inline
std::string to_string (Method m) {
    switch(m){
        case DEL:    return "DELETE";
        case GET:    return "GET";
        case PATCH:  return "PATCH";
        case POST:   return "POST";
        case PUT:    return "PUT";
        default:     return fmt::format("Method{}",static_cast<int>(m));
    }
}

namespace api {

struct EndPoint {
    std::string path;
    Method method;
    std::function<void(const Request& req, Response& res)> func;
    std::string brief;  // brief (1-liner) documentation
    std::string doc_md; // Markdown documentation (can be long)

    std::string ToHtml (void) const;
};

void markets_GET (const Request &req, Response &res);
void markets_id_GET (const Request &req, Response &res);
void markets_id_PUT (const Request &req, Response &res);
void markets_id_DEL (const Request &req, Response &res);
void markets_id_quotes_GET (const Request &req, Response &res);
void markets_id_quotes_id_GET (const Request &req, Response &res);
void markets_id_quotes_id_info_GET (const Request &req, Response &res);
void action_POST (const Request &req, Response &res);

} // namespace

enum HttpStatus {
    Continue = 100,
    SwitchingProtocol = 101,
//   case 102: return "Processing";
//   case 103: return "Early Hints";
    OK = 200,
    Created = 201,
//   case 201: return "Created";
//   case 202: return "Accepted";
//   case 203: return "Non-Authoritative Information";
//   case 204: return "No Content";
//   case 205: return "Reset Content";
//   case 206: return "Partial Content";
//   case 207: return "Multi-Status";
//   case 208: return "Already Reported";
//   case 226: return "IM Used";
//   case 300: return "Multiple Choice";
//   case 301: return "Moved Permanently";
//   case 302: return "Found";
//   case 303: return "See Other";
//   case 304: return "Not Modified";
//   case 305: return "Use Proxy";
//   case 306: return "unused";
//   case 307: return "Temporary Redirect";
//   case 308: return "Permanent Redirect";
//   case 400: return "Bad Request";
    BadRequest = 400,
//   case 401: return "Unauthorized";
//   case 402: return "Payment Required";
//   case 403: return "Forbidden";
    NotFound = 404,
//   case 405: return "Method Not Allowed";
//   case 406: return "Not Acceptable";
//   case 407: return "Proxy Authentication Required";
//   case 408: return "Request Timeout";
    Conflict = 409,
//   case 410: return "Gone";
//   case 411: return "Length Required";
//   case 412: return "Precondition Failed";
//   case 413: return "Payload Too Large";
//   case 414: return "URI Too Long";
//   case 415: return "Unsupported Media Type";
//   case 416: return "Range Not Satisfiable";
//   case 417: return "Expectation Failed";
//   case 418: return "I'm a teapot";
//   case 421: return "Misdirected Request";
//   case 422: return "Unprocessable Entity";
//   case 423: return "Locked";
//   case 424: return "Failed Dependency";
//   case 425: return "Too Early";
//   case 426: return "Upgrade Required";
//   case 428: return "Precondition Required";
//   case 429: return "Too Many Requests";
//   case 431: return "Request Header Fields Too Large";
//   case 451: return "Unavailable For Legal Reasons";
//   case 501: return "Not Implemented";
//   case 502: return "Bad Gateway";
//   case 503: return "Service Unavailable";
//   case 504: return "Gateway Timeout";
//   case 505: return "HTTP Version Not Supported";
//   case 506: return "Variant Also Negotiates";
//   case 507: return "Insufficient Storage";
//   case 508: return "Loop Detected";
//   case 510: return "Not Extended";
//   case 511: return "Network Authentication Required";

//   default:
    InternalServerError = 500
};
