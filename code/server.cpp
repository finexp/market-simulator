#include "httplib.h"
using namespace httplib;

#include "json.hpp"
using namespace nlohmann;

#include "log.hpp"
#include "env.hpp"
#include "api.hpp"
#include "utils.hpp"

int main(void) try {

    // read logging setup info
    spdlog::cfg::helpers::load_levels(env(
        "LOGGING",
        "debug", // "off,loggername=debug"
        [&] (const std::string &result) {
            std::cout << result << "\n";
        }
    ));

    Server svr;

    svr.set_logger([](const Request& req, const Response& res) {
        info("Server: {} {} #params={} res.status={}",req.path,req.method,req.params.size(),res.status);
    });
    
    svr.set_default_headers({{"Access-Control-Allow-Origin","*"}});
    // svr.set_payload_max_length(env<int>("MC_SERVER_PAYLOAD_LIMIT",1024));

    svr.set_exception_handler([](
        const Request & req,
        Response & res,
        std::exception & e
    ) {
        const auto exceptions_list = get_exceptions(e);
        // warning("set_exception_handler [len={}]: {}",exceptions_list.size(),e.what());
        std::string exceptions_trace; // = "\n";
        // std::string indent = "";
        for(const auto &s: exceptions_list){
            exceptions_trace += "\n    " + s;
        }
        warning("{}",exceptions_trace);
        // error("Error {} with {} headers",res.status,req.headers.size());
        // for (auto h: req.headers)
        //     error("  HEADER: {}: {}", h.first, h.second);
        res.set_content(fmt::format("{}",exceptions_trace),"text/plain");
        // res.reason = e.what();
        res.status = HttpStatus::InternalServerError;
    });

    svr.set_error_handler([](const Request & req, Response &res) {
        warning("set_error_handler: server is returning status={} reason: {}",res.status,res.reason);
        // error("Error {} with {} headers",res.status,req.headers.size());
        // for (auto h: req.headers)
        //     error("  HEADER: {}: {}", h.first, h.second);
        // res.set_content(fmt::format("{}",res.status),"text/plain");
    });

    constexpr auto id = R"(([\d\w\-_]{1,32}))";

    std::vector<api::EndPoint> endpoints {{
        .path   = R"(^/markets$)",
        .method = GET,
        .func   = api::markets_GET,
        .brief  = "List of market names with some minimal extra info",
        .doc_md = R"()"
    },{
        .path   = fmt::format("^/markets/{}$",id),
        .method = GET,
        .func   = api::markets_id_GET,
        .brief  = "Give info for the market ID",
        .doc_md = R"()"
    },{
        .path   = fmt::format("^/markets/{}$",id),
        .method = PUT,
        .func   = api::markets_id_PUT,
        .brief  = "Add the new market ID.",
        .doc_md = R"()"
    },{
        .path   = fmt::format("^/markets/{}$",id),
        .method = DEL,
        .func   = api::markets_id_DEL,
        .brief  = "Delete marketId",
        .doc_md = ""
    },{
        .path   = fmt::format("^/markets/{}/quotes$",id),
        .method = GET,
        .func   = api::markets_id_quotes_GET,
        .brief  = "List of quoteIds of the marketId",
        .doc_md = R"()"
    },{
        .path   = fmt::format("^/markets/{}/quotes/{}$",id,id),
        .method = GET,
        .func   = api::markets_id_quotes_id_GET,
        .brief  = "List of quotes for the given quotesId of the marketId",
        .doc_md = R"()"
    },{
        .path   = fmt::format("^/markets/{}/quotes/{}/info$",id,id),
        .method = GET,
        .func   = api::markets_id_quotes_id_info_GET,
        .brief  = "Info on quotes for the given quotesId of the marketId",
        .doc_md = R"()"
//    },{
//        .path   = "/stop",
//        .method = GET,
//        .func   = [&] (const Request& req, Response& res) {
//            svr.stop();
//        },
//        .brief  = "Stop the server",
//        .doc_md = ""
    },{
        .path   = "/action",
        .method = POST,
        .func   = api::action_POST,
        .brief  = "Action, which is based on the passed json payload.",
        .doc_md = ""
    }};

    svr.Get("/",[&endpoints](const Request &req, Response &res){

        std::string style = R"(
.brief {
    color: darkblue;
}
.doc-md {
    background-color: lightblue;
}
)";

        std::string body = "";
        // body += "<ul>\n";
        // for(const auto &e: endpoints){
        //     body += fmt::format("<li>{}</li>",e.to_html());
        // }
        // body += "</ul>\n";
        for(const auto &e: endpoints)
            body += fmt::format("{}\n",e.ToHtml());

        res.set_content(create_html_page(body,style), "text/html");
    });

    for(const auto &e: endpoints) {
        switch(e.method){
            case DEL: {
                svr.Delete(e.path,e.func);
                break;
            }
            case GET: {
                svr.Get(e.path,e.func);
                break;
            }
            case POST: {
                svr.Post(e.path,e.func);
                break;
            }
            case PUT: {
                svr.Put(e.path,e.func);
                break;
            }
            default:
                throw std::logic_error("Internal error, bad endpoint method");
        }
    }

    auto env_log = [&] (const std::string &result) {
            info("{}",result);
    };
    auto url  = env("MC_SERVER_URL" ,"0.0.0.0",env_log);
    auto port = env("MC_SERVER_PORT",8080,env_log);

    svr.listen(url,port);
    info("The end.");

} catch (const std::exception &e) {
    error("{}",e.what());
} catch (...) {
    error("Unknown exception");
}
